package com.zuitt.activity1;

import java.util.Scanner;



public class UserInput {
    public static void main(String[] args) {
        // Declare variables
        String firstName, lastName;
        double firstSubject, secondSubject, thirdSubject;

        // Create Scanner object
        Scanner scanner = new Scanner(System.in);

        // Get user's input
        System.out.print("Enter your first name: ");
        firstName = scanner.nextLine();

        System.out.print("Enter your last name: ");
        lastName = scanner.nextLine();

        System.out.print("Enter your grade for the first subject: ");
        firstSubject = scanner.nextDouble();

        System.out.print("Enter your grade for the second subject: ");
        secondSubject = scanner.nextDouble();

        System.out.print("Enter your grade for the third subject: ");
        thirdSubject = scanner.nextDouble();

        // Calculate average
        double average = (firstSubject + secondSubject + thirdSubject) / 3;

        // Print user's full name and average grade
        System.out.println("Good day, " + firstName + " " + lastName);
        System.out.println("Your grade average is: " + average);


    }
}


